<?php
namespace Daanvanberkel\Entity;

use Dotenv\Dotenv;

/**
 * Class User
 * @package         Huishoudboek
 * @subpackage      Entity
 * @author          Daan van Berkel <info@daanvanberkel.nl>
 * @license         MIT
 */
class User {
    private $id_user            = 0;
    private $firstname          = "";
    private $lastname           = "";
    private $email              = "";
    private $activated          = false;
    private $activation_token   = "";
    private $remember_token     = "";

    public function __construct(array $user = array()) {
        $dotenv = new Dotenv(getcwd());
        $dotenv->load();

        $dotenv->required(array('MYSQL_HOST', 'MYSQL_USER', 'MYSQL_PASS', 'MYSQL_DATABASE'));

        $db_host    = getenv('MYSQL_HOST');
        $db_user    = getenv('MYSQL_USER');
        $db_pass    = getenv('MYSQL_PASS');
        $db_name    = getenv('MYSQL_DATABASE');

        $this->db = new \PDO("mysql:host=" . $db_host . ";dbname=" . $db_name, $db_user, $db_pass);

        if (!empty($user)) {
            foreach($user as $key => $value) {
                // Change key to camelcase, ex. id_user -> IdUser
                $key = explode("_", $key);

                array_walk($key, function(&$item, $k) {
                    $item = ucfirst(strtolower($item));
                });

                $key = implode("", $key);

                // Method name will be, for example, setIdUser
                $method = "set" . ucfirst(strtolower($key));

                if (method_exists($this, $method) && !is_null($value)) {
                    $this->$method($value);
                }
            }

            return $this;
        }
    }

    /**
     * Check if user object is new
     *
     * @return      bool
     */
    public function isNew(): bool {
        return ($this->id_user == 0);
    }

    /**
     * Save user data to database, if id_user is 0 (zero) a new user will be saved to the database, an activation_token,
     * will also be generated
     *
     * @return  bool
     * @throws  \Exception
     */
    public function save(): bool {
        if (
            empty($this->getFirstname()) ||
            empty($this->getLastname()) ||
            empty($this->getEmail())
        ) {
            throw new \Exception("Not all required data is set for this user");
        }

        $query = "";
        $params = array();

        if ($this->isNew()) {
            $this->setActivationToken(bin2hex(random_bytes(20)));

            $query = "
                INSERT INTO
                    users (
                        firstname,
                        lastname,
                        email,
                        activation_token
                    )
                VALUES (
                    :firstname,
                    :lastname,
                    :email,
                    :activation_token
                );
            ";

            $params[":firstname"] = $this->getFirstname();
            $params[":lastname"] = $this->getLastname();
            $params[":email"] = $this->getEmail();
            $params[":activation_token"] = $this->getActivationToken();
        } else {
            $query = "
                UPDATE
                    users
                SET
                    firstname = :firstname,
                    lastname = :lastname,
                    email = :email,
                    activated = :activated,
                    activation_token = :activation_token,
                    remember_token = :remeber_token,
                WHERE
                    id_user = :id_user
            ";

            $params[":firstname"] = $this->getFirstname();
            $params[":lastname"] = $this->getLastname();
            $params[":email"] = $this->getEmail();
            $params[":activated"] = $this->getActivated();
            $params[":activation_token"] = $this->getActivationToken();
            $params[":remember_token"] = $this->getRememberToken();
            $params[":id_user"] = $this->getIdUser();
        }

        $stmt = $this->db->prepare($query);
        if ($stmt->execute($params) === false) {
            throw new \Exception($stmt->errorInfo(), $stmt->errorCode());
        }

        return true;
    }

    /**
     * Check user password
     *
     * @param   string      $password       The password to check
     * @return  bool                        True if password is correct, otherwise false
     */
    public function checkPassword(string $password): bool {
        $stmt = $this->db->prepare("
            SELECT
                secret,
                password
            FROM
                users
            WHERE
                id_user = :id
            LIMIT 1;
        ");

        if ($stmt->execute(array(":id" => $this->getIdUser())) === false) {
            return false;
        }

        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (count($user) < 1) {
            return false;
        }

        $db_secret = $user['secret'];
        $db_password = $user['password'];

        return password_verify($password . $db_secret, $db_password);
    }

    // ----------------------------- SETTERS -----------------------------

    public function setIdUser(int $id) {
        $this->id_user = $id;
    }

    public function setFirstname(string $name) {
        $this->firstname = $name;
    }

    public function setLastname(string $name) {
        $this->lastname = $name;
    }

    public function setEmail(string $email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
        }
    }

    public function setActivated($active) {
        $active = (bool) $active;

        $this->activated = $active;
    }

    public function setActivationToken(string $token) {
        $this->activation_token = $token;
    }

    public function setRememberToken(string $token) {
        $this->remember_token = $token;
    }

    // ----------------------------- GETTERS -----------------------------

    public function getIdUser(): int {
        return $this->id_user;
    }

    public function getFirstname(): string {
        return $this->firstname;
    }

    public function getLastname(): string {
        return $this->lastname;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function getActivated(): bool {
        return $this->activated;
    }

    public function getActivationToken(): string {
        return $this->activation_token;
    }

    public function getRememberToken(): string {
        return $this->remember_token;
    }

    public function getFullname(): string {
        return $this->getFirstname() . " " . $this->getLastname();
    }
}