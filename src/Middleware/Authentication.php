<?php
namespace Daanvanberkel\Middleware;

use Daanvanberkel\Model\User;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class Authentication middleware
 * @package         Huishoudboek
 * @subpackage      Middleware
 * @author          Daan van Berkel <info@daanvanberkel.nl>
 * @license         MIT
 */
class Authentication {
    public function __invoke(Request $request, Response $response, callable $next): Response {
        // Get session data
        $loggedin = (bool) ($_SESSION['loggedin'] ?? false);
        $id_user = (int) ($_SESSION['id_user'] ?? 0);

        // Check if user is logged in, otherwise send to /login
        if ($loggedin !== true || empty($id_user)) {
            return $response->withAddedHeader("Location", "/login");
        }

        // Try to get user from database
        try {
            $user = User::getInstance()->getUserById($id_user);
        } catch (\Exception $e) {
            $_SESSION['authtentication_message'] = $e->getMessage();
            return $response->withAddedHeader("Location", "/login");
        }

        // Check if user is activated
        if (!$user->getActivated()) {
            $_SESSION['authtentication_message'] = "User is not yet activated";
            return $response->withAddedHeader("Location", "/login");
        }

        // Add user to request
        $request = $request->withAttribute('user', $user);

        // Execute next middleware
        return $next($request, $response);
    }
}
