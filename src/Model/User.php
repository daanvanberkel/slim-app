<?php
namespace Daanvanberkel\Model;

use Dotenv\Dotenv;

/**
 * Class User
 * @package         Huishoudboek
 * @subpackage      Model
 * @author          Daan van Berkel <info@daanvanberkel.nl>
 * @license         MIT
 */
class User {
    private $db;
    private static $instance;

    public function __construct() {
        $dotenv = new Dotenv(getcwd());
        $dotenv->load();

        $dotenv->required(array('MYSQL_HOST', 'MYSQL_USER', 'MYSQL_PASS', 'MYSQL_DATABASE'));

        $host   = getenv('MYSQL_HOST');
        $user   = getenv('MYSQL_USER');
        $pass   = getenv('MYSQL_PASS');
        $db     = getenv('MYSQL_DATABASE');

        $this->db = new \PDO("mysql:host=" . $host . ";dbname=" . $db, $user, $pass);
    }

    /**
     * Get user from database by email
     *
     * @param       string                          $email          User email
     * @return      \Daanvanberkel\Entity\User                      User object
     * @throws      \Exception
     */
    public function getUserByEmail(string $email): \Daanvanberkel\Entity\User {
        // Is the supplied email valid?
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("Mailadres is not valid");
        }

        // Prepare and execute the query
        $stmt = $this->db->prepare("
            SELECT
                id_user,
                firstname,
                lastname,
                email,
                activated,
                activation_token,
                remember_token
            FROM
                users
            WHERE
                email = :email
        ");
        if ($stmt->execute(array(":email" => $email)) === false) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        // Get user object
        $users = $this->parseUsers($stmt->fetchAll(\PDO::FETCH_ASSOC));

        // No user found
        if (empty($users)) {
            throw new \Exception("User not found", 404);
        }

        return $users[0];
    }

    /**
     * Get user from database by id
     *
     * @param       int                             $id         User id
     * @return      \Daanvanberkel\Entity\User                  User object
     * @throws      \Exception
     */
    public function getUserById(int $id): \Daanvanberkel\Entity\User {
        // Is a id supplied?
        if (empty($id)) {
            throw new \Exception("Id is not valid");
        }

        // Prepare and execute the query
        $stmt = $this->db->prepare("
            SELECT
                id_user,
                firstname,
                lastname,
                email,
                activated,
                activation_token,
                remember_token
            FROM
                users
            WHERE
                id_user = :id
        ");
        if ($stmt->execute(array(":id" => $id)) === false) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        // Get user object
        $users = $this->parseUsers($stmt->fetchAll(\PDO::FETCH_ASSOC));

        // No user found
        if (empty($users)) {
            throw new \Exception("User not found", 404);
        }

        return $users[0];
    }

    /**
     * Get all users from the database
     *
     * @return      array           Array with \Daanvanberkel\Entity\User objects
     * @throws      \Exception
     */
    public function getUsers(): array {
        // Prepare and execute the query
        $stmt = $this->db->prepare("
            SELECT
                id_user,
                firstname,
                lastname,
                email,
                activated,
                activation_token,
                remember_token
            FROM
                users
        ");
        if ($stmt->execute() === false) {
            throw new \Exception(implode(PHP_EOL, $stmt->errorInfo()));
        }

        // Get user objects
        $users = $this->parseUsers($stmt->fetchAll(\PDO::FETCH_ASSOC));

        return $users;
    }

    /**
     * Parse associated array to user objects
     *
     * @param       array           $users          Associated array with users
     * @return      array                           Array with \Daanvanberkel\Entity\User objects
     */
    private function parseUsers(array $users): array {
        $output = array();

        foreach ($users as $user) {
            if (!is_array($user)) {
                continue;
            }

            $tmp = new \Daanvanberkel\Entity\User($user);

            $output[] = $tmp;
            unset($tmp);
        }

        return $output;
    }

    /**
     * Get user model instance
     *
     * @return User
     */
    public static function getInstance(): self {
        if (empty(self::$instance) || !(self::$instance instanceof self)) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}