<?php
namespace Daanvanberkel;

class Router {
    public static function addRoutes(&$app) {
        if (
            !($app instanceof \Slim\App)
        ) {
            throw new \TypeError("App variable is no instance of \\Slim\\App or \\Daanvanberkel\\App");
        }

        $path = getcwd() . '/config/routes.json';

        if (!file_exists($path)) {
            throw new \Exception("Route config file cannot be found at " . $path);
        }

        try {
            $routes = json_decode(file_get_contents($path));
        } catch (\Exception $e) {
            throw new \Exception("Error while decoding JSON", $e->getCode(), $e);
        }

        $valid_http_methods = array(
            "get",
            "post",
            "put",
            "delete"
        );

        foreach($routes as $route) {
            if (
                !isset($route->http_method) ||
                !isset($route->uri) ||
                !isset($route->method)
            ) {
                continue;
            }

            $route->http_method = strtolower($route->http_method);

            if (!in_array($route->http_method, $valid_http_methods)) {
                continue;
            }

            $class = explode(":", $route->method);
            $class = $class[0];

            if (!class_exists($class)) {
                continue;
            }

            $http_method = $route->http_method;

            if (class_exists('Daanvanberkel\Middleware\Authentication')) {
                if (isset($route->authentication) && $route->authentication === false) {
                    $app->$http_method($route->uri, $route->method);
                } else {
                    $app->$http_method($route->uri, $route->method)->add(new \Daanvanberkel\Middleware\Authentication());
                }
            } else {
                $app->$http_method($route->uri, $route->method);
            }
        }
    }
}